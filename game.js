//cupid game code author: Thomas Back
var GAME_WIDTH = 256;
var GAME_HEIGHT = 256;
var GAME_SCALE = 3;
var gameport = document.getElementById("gameport");

var renderer = PIXI.autoDetectRenderer(GAME_WIDTH, GAME_HEIGHT, {backgroundColor: 0x00BFFF});
gameport.appendChild(renderer.view);
var starttext = new PIXI.Text("Press Enter to play");
starttext.scale.x = 0.15;
starttext.scale.y = 0.15;
starttext.position.x = 100;
starttext.position.y = 100;
var endtext = new PIXI.Text("You won good job!");
endtext.scale.x = 0.15;
endtext.scale.y = 0.15;
endtext.position.x = 100;
endtext.position.y = 100;


var stage = new PIXI.Container();

stage.scale.x = GAME_SCALE;
stage.scale.y = GAME_SCALE;
var frames_left = [];
var frames_right = [];
var world;
var cupid;
var wall_layer;
var enemies = [];
var enemy_frame = [];
PIXI.SCALE_MODES.DEFAULT = PIXI.SCALE_MODES.NEAREST;
PIXI.loader
        .add('map_json','map.json')
        .add('map','map.png')
        .add('cupid_right.json')
        .add('cupid_left.json')
        .add('enemy_right.json')
        .load(ready);
var pellet = new PIXI.Sprite(PIXI.Texture.fromImage('pellet.png'));

var ingame = false;

function ready(){
    var tu = new TileUtilities(PIXI);
    world = tu.makeTiledWorld("map_json", "map.png");
    stage.addChild(world);

    
    for (i=1;i <=3;i++){
        frames_right.push(PIXI.Texture.fromFrame('cupid' + i +'.png'));
        frames_left.push(PIXI.Texture.fromFrame('cupid_left' + i + '.png'));
        enemy_frame.push(PIXI.Texture.fromFrame('enemy_right' + i + '.png'));
    }
    cupid = new PIXI.extras.MovieClip(frames_right);
    cupid.anchor.x = 0.5;
    cupid.anchor.y = 0.7;
    cupid.position.x = 100;
    cupid.position.y = 100;
    cupid.play();
    var entity_layer = world.getObject("players");
    wall_layer = world.getObject("boundaries").data;
    entity_layer.addChild(cupid);
    for (i = 0 ; i < 7; i ++){
        enemies[i] = new PIXI.extras.MovieClip(enemy_frame);
        enemies[i].anchor.x = 0.5;
        enemies[i].anchor.y = 0.5;
        enemies[i].position.x = Math.floor(Math.random() * 125) + 50;
        enemies[i].position.y = Math.floor(Math.random() * 125) + 50;
        enemies[i].play();
        entity_layer.addChild(enemies[i]);
    }
    pellet.x = cupid.position.x;
    pellet.y = cupid.position.y;
    pellet.visible = false;
    entity_layer.addChild(pellet);
    enemies_left();  
    entity_layer.addChild(starttext);
    entity_layer.addChild(endtext);
    endtext.visible = false;
}
document.addEventListener('keydown', keydownEventHandler);
document.addEventListener('keyup', keyupEventHandler);
var moveup = false;
var movedown = false;
var moveright = false;
var moveleft = false;
function box_point_intersection(box, x, y){
    if(box.position.x > x) return false;
    if (x > box.position.x + box.width) return false;
    if(box.position.y > y) return false;
    if ( y > box.position.y + box.height) return false;
    return true;
}
function enemies_left(){
    for( i = 0; i < 7 ; i++){
         createjs.Tween.get(enemies[i].position).to({x: enemies[i].position.x + 20}, 4000,createjs.Ease.cubicOut);
    }
    }
function shoot(){
    if(pellet.visible == true){
        pellet.x = cupid.position.x;
        pellet.y = cupid.position.y - 0.5;
        createjs.Tween.removeTweens(pellet.position);
        if(facing_right){
            createjs.Tween.get(pellet.position).to({x:pellet.position.x - 50},500);
        }
        else if(facing_left){
            createjs.Tween.get(pellet.position).to({x:pellet.position.x + 50},500);
        }
        
        
    }
}
var pellet_shoot = false;
function checkwin(){
    var win = 0;
    for(i = 0; i < 7; i++){
        if (enemies[i].alpha == 0){
            win += 1;
        }
    }
    if(win == 7){
        endtext.position.x = cupid.position.x - 10;
        endtext.position.y = cupid.position.y;
        return true;

    }
    else{
        return false;
    }
}
function keydownEventHandler(e){
    if(e.keyCode == 13 && ingame == false){
        starttext.alpha = 0;
        ingame = true;
    }
    if(ingame){
        if(e.keyCode == 69){
            console.log("hey");
            pellet.visible = true;
            shoot();
        }
    
        if (e.keyCode ==  87) {// w key

            moveup = true;
        }
        if (e.keyCode == 83) {

            movedown = true;
        }
        if (e.keyCode == 65){

            moveright = true;
        }
        if (e.keyCode == 68) {

            moveleft = true;
        }
    }
    
}
function keyupEventHandler(e){
    
    
    if (e.keyCode ==  87) {// w key
        
        moveup = false;
    }
    if (e.keyCode == 83) {
        
        movedown = false;
    }
    if (e.keyCode == 65){
       
        moveright = false;
    }
    if (e.keyCode == 68) {
        
        moveleft = false;
    }  
}
var facing_right = true;
var facing_left = false;
setInterval(player_input,15);
function player_input(){

    if(moveup  ){
        cupid.position.y -= 1;
        console.log(cupid.position.y);
    }
    if(movedown ){
       cupid.position.y += 1;
        console.log(cupid.position.y);
    }
    if(moveright ){
        facing_right = true;
        facing_left = false;
       cupid.position.x -= 1;
        console.log(cupid.position.x);
    }
    if(moveleft ){
        facing_left = true;
        facing_right = false;
       cupid.position.x += 1;
       
    }
    if(facing_left){
        cupid.scale.x = 1;
    }
    if(facing_right){
        cupid.scale.x = -1;
    }
    if(checkwin()){
        ingame = false;
        endtext.visible = true;
    }
}  
animate();


function animate(timestamp) {
  requestAnimationFrame(animate);
  update_camera();
  renderer.render(stage);
  for(i = 0; i < 7; i ++){
      if(box_point_intersection(enemies[i],pellet.x,pellet.y)){
          createjs.Tween.removeTweens(pellet.position);
          createjs.Tween.get(enemies[i]).to({alpha: 0}, 400);
          pellet.visible = false;
      }
  }
}
function update_camera() {
  stage.x = -cupid.x*GAME_SCALE + GAME_WIDTH/2 - cupid.width/2*GAME_SCALE;
  stage.y = -cupid.y*GAME_SCALE + GAME_HEIGHT/2 + cupid.height/2*GAME_SCALE;
  stage.x = -Math.max(0, Math.min(world.worldWidth*GAME_SCALE - GAME_WIDTH, -stage.x));
  stage.y = -Math.max(0, Math.min(world.worldHeight*GAME_SCALE - GAME_HEIGHT, -stage.y));
}


